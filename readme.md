# Instruções

---

## Ambiente

- sudo apt install python
- sudo apt install pipenv
- sudo apt install pip

# Startar ambiente

- pipenv shell (na pasta do projeto)
- pipenv install (na pasta do projeto)
- python .\app.py (rodar o projeto)
