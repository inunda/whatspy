# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request
from flask_restful import reqparse, abort, Api, Resource

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep
import simplejson as json
import socket
import base64
import time
import re

app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()


driver = webdriver.Chrome('chromedriver')
driver.get("https://web.whatsapp.com/")
time.sleep(15)


class QrCode(Resource):
    def get(self):
        driver.find_element_by_class_name(
            'landing-main').screenshot('code.png')

        with open("code.png", "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())

        codeqr = json.dumps({b'code64': encoded_string})

        return {
            "qrcode": codeqr
        }


class ViewContacts(Resource):
    def get(self):
        data = request.get_json(force=True)
        usertoken = data['usertoken']

        qr_code = driver.find_element_by_class_name(
            'landing-main').screenshot_as_base64

        if(usertoken != ''):
            users = driver.find_elements_by_class_name('X7YrQ')
            arruser = []
            for user in users:
                arruser.append(user.get_attribute('innerHTML'))
            numero = []
            nome = []
            foto = []
            unread = []
            for userinfo in range(int(len(arruser))):
                if (str(re.findall(
                        'class="P6z4j">(.*)</span></div>', arruser[userinfo])) != '[]'):
                    unread.append(str(re.findall(
                        'class="P6z4j">(.*)</span></div>', arruser[userinfo])))
                    numero.append(
                        str(re.findall('u=(.*)%40c.', arruser[userinfo])))
                    nome.append(
                        str(re.search('dir="auto" title="(.*)" class="_19RFN"', arruser[userinfo]).group(1)))
                    foto.append(
                        str(re.findall('<img src="(.*)" draggable="false"', arruser[userinfo])))
            return {
                "numero": numero,
                "nomes": nome,
                'foto': foto,
                'unread': unread,
                "id": usertoken,
                "qr": qr_code}
        else:
            return {
                "error": 'true'
            }


class Read(Resource):
    def post(self):
        data = request.get_json(force=True)
        username = data['username']

        chat = username
        usermsgs = []

        time.sleep(10)
        driver.find_element_by_xpath(
            "//span[@title='" + chat + "']").click()
        msgs = driver.find_elements_by_css_selector('div.message-in')
        unreads = driver.find_elements_by_css_selector('span.P6z4j')

        for msg in msgs:
            usermsgs.append(msg.get_attribute('textContent'))

        return {
            "usermsgs": usermsgs
        }


class Send(Resource):
    def get(self):

        data = request.get_json(force=True)
        usermsg = data['usermsg']
        usernum = data['usernum']

        message_text = usermsg
        no_of_message = 1
        moblie_no_list = usernum

        def element_presence(by, xpath, time):
            element_present = EC.presence_of_element_located((By.XPATH, xpath))
            WebDriverWait(driver, time).until(element_present)

            def is_connected():
                def send_whatsapp_msg(phone_no, text):
                    driver.get(
                        "https://web.whatsapp.com/send?phone={}".format(phone_no))
                try:
                    element_presence(
                        By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]', 30)
                    txt_box = driver.find_element(
                        By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
                    global no_of_message
                    for x in range(no_of_message):
                        txt_box.send_keys(text)
                        txt_box.send_keys("\n")

                except Exception as e:
                    print("invailid phone no :"+str(phone_no))
                    for moblie_no in moblie_no_list:
                        try:
                            send_whatsapp_msg(moblie_no, message_text)
                        except Exception as e:
                            sleep(10)
                            is_connected()

        return {
            "sucesso": "true"
        }


api.add_resource(ViewContacts, '/')
api.add_resource(Read, '/')
api.add_resource(Send, '/')
api.add_resource(QrCode, '/code')

if __name__ == '__main__':
    app.run(debug=False)
